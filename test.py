# -*- coding: utf-8 -*
import os  
import time

# [Content] XML Footer Text
def test_hasHomePageNode():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('首頁') != -1

# [Behavior] Tap the coordinate on the screen
def test_tapSidebar():
	os.system('adb shell input tap 100 100')

# 1. [Content] Side Bar Text
def test_hasSideBarText():
    os.system('adb shell input tap 84 145 && adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
    with open('window_dump.xml', 'r', encoding="utf-8") as f:
        xmlString = f.read()
        assert xmlString.find('查看商品分類') != -1


# 2. [Screenshot] Side Bar Text
def test_2():
  os.system('adb shell input tap 84 145 && adb shell screencap -p /sdcard/screen.png && adb pull /sdcard/screen.png && mv ./screen.png ./screenshots/2.png')
  assert(os.path.isfile('./screenshots/2.png'))
# 3. [Context] Categories
def test_3():
    os.system('adb shell input swipe 100 1500 100 200') # swipe to show Categories button
    os.system('sleep 2')
    os.system('adb shell input tap 992 282')
    os.system('sleep 2')
    os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
    f = open('window_dump.xml', 'r', encoding="utf-8")
    xmlString = f.read()
    os.system('adb shell input tap 640 1250') # close the window
    os.system('sleep 2')


# 4. [Screenshot] Categories
def test_4():
  os.system('adb shell input swipe 500 1000 300 300 && adb shell input tap 1000 600 && adb shell screencap -p /sdcard/screen.png && adb pull /sdcard/screen.png && mv ./screen.png ./screenshots/4.png')
  assert(os.path.isfile('./screenshots/4.png'))
# 5. [Context] Categories page
def test_5():
    os.system('adb shell input tap 100 100')
    os.system('adb shell input tap 150 600') # where the Categories tap is
    os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
    f = open('window_dump.xml', 'r', encoding="utf-8")
    xmlString = f.read()
    assert xmlString.find('24H購物') != -1
    assert xmlString.find('購物中心') != -1
    os.system('adb shell input keyevent 4')
    os.system('sleep 3')

# 6. [Screenshot] Categories page
def test_6():
    os.system('adb shell input tap 100 100')
    os.system('sleep 5')
    os.system('adb shell input tap 150 600') # where the Categories tap is
    os.system('sleep 2')
    os.system('adb shell screencap -p /sdcard/CategoriesPage.png')
    os.system('adb pull /sdcard/CategoriesPage.png .')
    os.system('mv ./CategoriesPage.png ./screenshots/6.png')

# 7. [Behavior] Search item “switch”
def test_7():
    os.system('adb shell input tap 426 127')
    os.system('sleep 2')
    os.system('adb shell input text "switch"')
    os.system('sleep 1.5')
    os.system('adb shell input keyevent 66') # 66 for enter
    os.system('sleep 3')

# 8. [Behavior] Follow an item and it should be add to the list
def test_8():
    os.system('adb shell input tap 600 1050')
    os.system('sleep 5')
    os.system('adb shell input tap 100 1716')
    os.system('sleep 3')
    os.system('adb shell input keyevent 4')
    os.system('sleep 3')
    os.system('adb shell input keyevent 4')
    os.system('sleep 3')
    os.system('adb shell input keyevent 4')
    os.system('sleep 3')
    os.system('adb shell input tap 100 100')
    os.system('sleep 5')
    os.system('adb shell input tap 258 849')
    os.system('sleep 5')

# 9. [Behavior] Navigate tto the detail of item
def test_9():
    os.system('adb shell input tap 241 766')
    os.system('sleep 4')
    os.system('adb shell input swipe 500 1469 500 331')
    os.system('sleep 3')
    os.system('adb shell input tap 549 145')
    os.system('sleep 3')


# 10. [Screenshot] Disconnetion Screen
def test_10():
    os.system('adb shell svc wifi disable')
    os.system('sleep 2')
    os.system('adb shell screencap -p /sdcard/Disconnection.png')
    os.system('adb pull /sdcard/Disconnection.png .')
    os.system('mv ./Disconnection.png ./screenshots/10.png')

